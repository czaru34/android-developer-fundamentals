package com.example.alarmhomework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private final String notificationChannelId =
            "NOTIFICATION_ID";
    private final int notificationId = 0;

    NotificationManager notMan;
    ToggleButton togBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        togBut = findViewById(R.id.toggle_button);
        notMan = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel();
        togBut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    sendNotification(MainActivity.this);
                } else {
                    notMan.cancelAll();
                }
            }
        });
    }

    public void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel =
                    new NotificationChannel(notificationChannelId, "Time notification", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableVibration(true);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.setDescription("Shows time notification");
            notMan.createNotificationChannel(notificationChannel);
        }
    }

    public void sendNotification(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("h:mm a");
            String currentTime = simpleDateFormat.format(new Date());
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, notificationChannelId)
                    .setAutoCancel(true)
                    .setContentTitle("It's time!")
                    .setSmallIcon(R.drawable.ic_time)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentText(currentTime);
                    //.setContentText(Duration.ofMillis(SystemClock.elapsedRealtime()).toString());
            notMan.notify(notificationId, builder.build());
        }
    }
}
