package com.example.notificationscheduler;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

public class NotificationJobService extends JobService {
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private NotificationManager mNotifyManager;
    private JobParameters mParams;

    public void createNotificationChannel() {
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel jobNotificationChannel= new NotificationChannel(PRIMARY_CHANNEL_ID, "Job Service Notification", NotificationManager.IMPORTANCE_HIGH);
            jobNotificationChannel.enableLights(true);
            jobNotificationChannel.enableVibration(true);
            jobNotificationChannel.setDescription("Notifications from Job Service");
            jobNotificationChannel.setLightColor(Color.GREEN);
            mNotifyManager.createNotificationChannel(jobNotificationChannel);
        }
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        mParams = params;
        createNotificationChannel();
        PendingIntent contentPendingIntent =
                PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_job_running)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_text))
                .setContentIntent(contentPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setAutoCancel(true);
        new SimpleSleepTask(getApplicationContext()).execute();
        mNotifyManager.notify(0, notificationBuilder.build());
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    public class SimpleSleepTask extends AsyncTask<Void, Void, Boolean> {
        private Context mContext;
        SimpleSleepTask(Context context) { mContext = context; }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Thread.sleep(10000);
                return true;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(mContext, "Job is finished.", Toast.LENGTH_SHORT).show();
                jobFinished(mParams, false);
            } else {
                Toast.makeText(mContext, "Reselect constraints.", Toast.LENGTH_SHORT).show();
                jobFinished(mParams, true);
            }
        }
    }
}
