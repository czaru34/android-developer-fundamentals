package com.example.notificationscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private JobScheduler mScheduler;
    private Switch mIdleSwitch;
    private Switch mChargingSwitch;
    private SeekBar mSeekBar;

    private static final int JOB_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIdleSwitch = findViewById(R.id.idleSwitch);
        mChargingSwitch = findViewById(R.id.chargingSwitch);
        mSeekBar = findViewById(R.id.seekBar);
        final TextView seekBarProgress = findViewById(R.id.override_value);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress > 0)
                    seekBarProgress.setText(progress + "s");
                else
                    seekBarProgress.setText("Not set");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void scheduleJob(View view) {
        RadioGroup mRadioGroup = findViewById(R.id.radio_group);
        int selectedNetworkID = mRadioGroup.getCheckedRadioButtonId();
        int selectedNetworkOption = JobInfo.NETWORK_TYPE_NONE;
        switch(selectedNetworkID) {
            case R.id.none_network:
                selectedNetworkOption = JobInfo.NETWORK_TYPE_NONE;
                break;
            case R.id.any_network:
                selectedNetworkOption = JobInfo.NETWORK_TYPE_ANY;
                break;
            case R.id.wifi_network:
                selectedNetworkOption = JobInfo.NETWORK_TYPE_UNMETERED;
                break;
        }

        mScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        ComponentName serviceName = new ComponentName(getPackageName(), NotificationJobService.class.getName());
        JobInfo.Builder jobInfoBuilder = new JobInfo.Builder(JOB_ID, serviceName)
                .setRequiredNetworkType(selectedNetworkOption)
                .setRequiresDeviceIdle(mIdleSwitch.isChecked())
                .setRequiresCharging(mChargingSwitch.isChecked());
        int seekBarInteger = mSeekBar.getProgress();
        boolean seekBarSet = seekBarInteger > 0;
        if(seekBarSet)
            jobInfoBuilder.setOverrideDeadline(seekBarInteger * 1000);
        boolean constraintSet = selectedNetworkOption != JobInfo.NETWORK_TYPE_NONE || mChargingSwitch.isChecked() || mIdleSwitch.isChecked() || seekBarSet;
        if(constraintSet) {
            mScheduler.schedule(jobInfoBuilder.build());
            Toast.makeText(this, "Job scheduled.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Please set at least one of the following constraints.", Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelJobs(View view) {
        if(mScheduler != null) {
            mScheduler.cancelAll();
            mScheduler = null;
            Toast.makeText(this, "Jobs canceled.", Toast.LENGTH_SHORT).show();
        }
    }
}
