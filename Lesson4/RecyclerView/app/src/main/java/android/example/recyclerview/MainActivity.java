package android.example.recyclerview;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    LinkedList<String> mWordList;

    RecyclerView mRecyclerView;
    WordListAdapter mWordListAdapter;
    private int gridLayoutCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               int wordListSize = mWordList.size();
               mWordList.addLast("Word " + wordListSize);
               mRecyclerView.getAdapter().notifyItemInserted(wordListSize);
               mRecyclerView.smoothScrollToPosition(wordListSize);
            }
        });

        gridLayoutCount = getResources().getInteger(R.integer.grid_layout_value);

        mWordList = fillListWithWords();
        showList();
    }

    public void showList() {
        mRecyclerView = findViewById(R.id.recyclerview);
        mWordListAdapter = new WordListAdapter(getApplicationContext(), mWordList);
        mRecyclerView.setAdapter(mWordListAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, gridLayoutCount));
    }

    public LinkedList <String> fillListWithWords() {
        LinkedList<String> tmp = new LinkedList<>();
        for(int i = 0; i < 20; i++)
            tmp.addLast("Word" + i);
        return tmp;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_reset){
            mWordList.clear();
            mWordList = fillListWithWords();
            showList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
