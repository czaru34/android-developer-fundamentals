package android.example.pickerfordate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void processDatePickerResult(int year, int month, int day) {
        String month_s = Integer.toString(month+1);
        String day_s = Integer.toString(day);
        String year_s = Integer.toString(year);
        String date_message = day_s + "/" + month_s + "/" + year_s;

        Toast.makeText(getApplicationContext(), date_message, Toast.LENGTH_SHORT).show();
    }
}
