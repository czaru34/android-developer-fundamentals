package android.example.checkboxes_and_etc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toast = "Toppings:";
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        switch(view.getId()){
            case R.id.chocolate_syrup:
                if(checked)
                    toast += getString(R.string.chocolate_syrup) + " ";
                else {
                    toast = toast.replaceFirst(getString(R.string.chocolate_syrup), "");
                }
                break;
            case R.id.sprinkles:
                if(checked)
                    toast += getString(R.string.sprinkles) + " ";
                else {
                    toast = toast.replaceFirst(getString(R.string.sprinkles), "");
                }
                break;
            case R.id.crushed_nuts:
                if(checked)
                    toast += getString(R.string.crushed_nuts) + " ";
                else {
                    toast = toast.replaceFirst(getString(R.string.crushed_nuts), "");
                }
                break;
            case R.id.cherries:
                if(checked)
                    toast += getString(R.string.cherries) + " ";
                else {
                    toast = toast.replaceFirst(getString(R.string.cherries), "");
                }
                break;
            case R.id.orios:
                if(checked)
                    toast += getString(R.string.orios) + " ";
                else {
                    toast = toast.replaceFirst(getString(R.string.orios), "");
                }
                break;
            default:
                toast = "Toppings: ";
                break;
        }
    }

    public void displayToast(View view) {
        Toast.makeText(getApplicationContext(), toast, Toast.LENGTH_SHORT).show();
    }
}
