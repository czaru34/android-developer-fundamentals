package com.example.toolbarhomework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
    }

    public void start1(View view) {
        Intent intent  = new Intent(MainActivity.this, start1Activity.class);
    }

    public void start2(View view) {
        Intent intent  = new Intent(MainActivity.this, start2Activity.class);
    }

    public void start3(View view) {
        Intent intent  = new Intent(MainActivity.this, start3Activity.class);
    }
}
