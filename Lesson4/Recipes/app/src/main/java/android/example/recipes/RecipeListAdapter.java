package android.example.recipes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder> {
    private final LinkedList<String> recipeList;
    private final LayoutInflater mInflater;
    private Context mContext;

    class RecipeViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        public final TextView mTextView;
        final RecipeListAdapter mAdapter;

        public RecipeViewHolder(View itemView, RecipeListAdapter adapter) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.recipe_title);
            this.mAdapter = adapter;
        }

        @Override
        public void onClick(View view) {
            Log.d("=======", Integer.toString(view.getId()));
        }
    }

    RecipeListAdapter(Context context, LinkedList<String> recipes) {
        this.recipeList = recipes;
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @NonNull
    @Override
    public RecipeListAdapter.RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recipe_title, parent, false);
        return new RecipeViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeListAdapter.RecipeViewHolder holder, final int position) {
        final String mCurrent = recipeList.get(position);
        holder.mTextView.setText(mCurrent);
        holder.mTextView.setTag(position);
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, RecipeActivity.class);
                intent.putExtra("Position", position);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }
}
