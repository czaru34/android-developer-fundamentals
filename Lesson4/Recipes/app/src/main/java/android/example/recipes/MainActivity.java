package android.example.recipes;

import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private LinkedList<String>recipeList;
    private RecyclerView mRecyclerView;
    private RecipeListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recipeList = fillRecipeList();
        mRecyclerView = findViewById(R.id.recycler_view);
        mAdapter = new RecipeListAdapter(this, recipeList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public LinkedList<String> fillRecipeList() {
        LinkedList<String>tmp = new LinkedList<>();
        tmp.addLast("Kotlety mielone");
        tmp.addLast("Kotlety schabowe");
        tmp.addLast("Rosół");
        return tmp;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
