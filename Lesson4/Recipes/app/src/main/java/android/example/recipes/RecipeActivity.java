package android.example.recipes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class RecipeActivity extends AppCompatActivity {
    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        mTextView = findViewById(R.id.recipe);
        Intent intent = getIntent();
        setRecipe(intent);
    }

    public void setRecipe(Intent intent) {
        Resources res = getResources();
        String[] recipes = res.getStringArray(R.array.recipes);

        switch(intent.getIntExtra("Position", 5)) {
            case 0:
                mTextView.setText(recipes[0]);
                break;
            case 1:
                mTextView.setText(recipes[1]);
                break;
            case 2:
                mTextView.setText(recipes[2]);
                break;
                default:
                    break;
        }
    }
}
