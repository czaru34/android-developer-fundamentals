package com.example.getwebpagesourcecode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    Spinner mProtocolSpinner;
    EditText mHostName;
    TextView mPageSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initVariables();
        initSpinner();
        if (getSupportLoaderManager().getLoader(0) != null)
            getSupportLoaderManager().initLoader(0, null, this);

    }

    public void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.protocol_choice, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mProtocolSpinner.setAdapter(adapter);
    }

    public void initVariables() {
        mProtocolSpinner = findViewById(R.id.protocol_spinner);
        mHostName = findViewById(R.id.host_name);
        mPageSource = findViewById(R.id.page_source);
    }

    public void getPageSource(View view) {
        String url = mProtocolSpinner.getSelectedItem().toString() + mHostName.getText().toString();

        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if(connMgr != null) {
            netInfo = connMgr.getActiveNetworkInfo();
        }

        if(netInfo != null && netInfo.isConnected() && url.length() != 0) {
            Bundle urlBundle = new Bundle();
            urlBundle.putString("url", url);
            getSupportLoaderManager().restartLoader(0, urlBundle, this);
            mPageSource.setText("Loading...");
        } else {
            if(url.length() == 0) {
                mPageSource.setText("Enter URL");
            } else {
                mPageSource.setText("Check internet connection");
            }
        }
    }

    @NonNull
    @Override
    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
        String url = null;
        if(args != null)
            url = args.getString("url");
        return new WebPageSourceLoader(this, url);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
        try {
            mPageSource.setText(data);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {

    }
}
