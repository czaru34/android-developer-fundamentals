package com.example.getwebpagesourcecode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkUtils {
    private HttpURLConnection urlConnection = null;
    private URL data = null;
    private BufferedReader reader = null;
    private String webPageSource;

    public String getPageSource(String url) {
        try {
            data = new URL(url);
            urlConnection = (HttpURLConnection) data.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = null;
            StringBuilder builder = new StringBuilder();
            int i = 0;
            while((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append("\n");
                i++;
                if(builder.length() == 0) return null;
            }
            webPageSource = builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //close reader and disconnect from api
            if(urlConnection != null) urlConnection.disconnect();
            if (reader != null)
                try { reader.close(); }
                catch(IOException e) { e.printStackTrace(); }
        }
        return webPageSource;
    }
}
