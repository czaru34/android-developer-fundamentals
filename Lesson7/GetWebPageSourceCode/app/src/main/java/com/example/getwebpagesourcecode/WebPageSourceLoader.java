package com.example.getwebpagesourcecode;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

public class WebPageSourceLoader extends AsyncTaskLoader<String> {
    private String mURLString;

    public WebPageSourceLoader(@NonNull Context context,  String urlString) { super(context); mURLString = urlString; }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Nullable
    @Override
    public String loadInBackground() {
        return new NetworkUtils().getPageSource(mURLString);
    }
}
