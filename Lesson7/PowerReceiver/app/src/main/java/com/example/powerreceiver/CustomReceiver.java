package com.example.powerreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CustomReceiver extends BroadcastReceiver {
    private static final String ACTION_CUSTOM_BROADCAST =
            BuildConfig.APPLICATION_ID + ".ACTION_CUSTOM_BROADCAST";
    private static final String LOCAL_CUSTOM_BROADCAST = BuildConfig.APPLICATION_ID + ".LOCAL_CUSTOM_BROADCAST";

    @Override
    public void onReceive(Context context, Intent intent) {
        String intentAction = intent.getAction();
        if(intentAction!=null) {
            String toastMessage;
            switch(intentAction) {
                case Intent.ACTION_POWER_CONNECTED:
                    toastMessage = "Power connected!";
                    break;
                case Intent.ACTION_POWER_DISCONNECTED:
                    toastMessage = "Power disconnected!";
                    break;
                case ACTION_CUSTOM_BROADCAST:
                    toastMessage = "Custom Action Broadcast Received";
                    break;
                case LOCAL_CUSTOM_BROADCAST:
                    toastMessage = "Custom Local Broadcast Received"
                            + "\n Square of the Random number: "
                            + Math.pow(intent.getIntExtra("integer", 0), 2);
                    break;
                case Intent.ACTION_HEADSET_PLUG:
                    toastMessage = "Headset plugged";
                    break;
                    default:
                        toastMessage = "unknown intent action";
                        break;
            }
            Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
        }
    }
}
