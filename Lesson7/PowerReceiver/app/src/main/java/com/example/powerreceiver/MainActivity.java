package com.example.powerreceiver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends AppCompatActivity  {
    private static final String ACTION_CUSTOM_BROADCAST =
            BuildConfig.APPLICATION_ID + ".ACTION_CUSTOM_BROADCAST";
    private static final String LOCAL_CUSTOM_BROADCAST = BuildConfig.APPLICATION_ID + ".LOCAL_CUSTOM_BROADCAST";
    private CustomReceiver mReceiver = new CustomReceiver();
    int randomInt;
    private TextView mIntegerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //IntentFilter filter = new IntentFilter();
        //filter.addAction(Intent.ACTION_POWER_CONNECTED);
        //filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        //filter.addAction(Intent.ACTION_HEADSET_PLUG);
        //this.registerReceiver(mReceiver, filter);

        Random rng = new Random();
        randomInt = rng.nextInt(20);
        mIntegerView = findViewById(R.id.integer);
        mIntegerView.setText(Integer.toString(randomInt));
        //LocalBroadcastManager.getInstance(this).registerReceiver
        //        (mReceiver, new IntentFilter(ACTION_CUSTOM_BROADCAST));
        //Homework
        LocalBroadcastManager.getInstance(this).registerReceiver
                (mReceiver, new IntentFilter(LOCAL_CUSTOM_BROADCAST));
    }

    @Override
    protected void onDestroy() {
        this.unregisterReceiver(mReceiver);
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    public void sendCustomBroadcast(View view) {
        //Intent customBroadcast = new Intent(ACTION_CUSTOM_BROADCAST);
        //Homework
        Intent customBroadcast = new Intent(LOCAL_CUSTOM_BROADCAST);
        customBroadcast.putExtra("integer", randomInt);
        LocalBroadcastManager.getInstance(this).sendBroadcast(customBroadcast);
    }
}
