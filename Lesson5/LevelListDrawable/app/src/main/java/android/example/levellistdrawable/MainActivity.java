package android.example.levellistdrawable;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private int imageLevel;
    private ImageButton batteryView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageLevel = 0;
        batteryView = this.<ImageButton>findViewById(R.id.battery);
    }

    public void increaseImageLevel(View view) {
        if(imageLevel >= 0) {
            if (imageLevel == 5) {}
            else {
                imageLevel++;
                batteryView.setImageLevel(imageLevel);
            }
        }
    }

    public void decreaseImageLevel(View view) {
        if(imageLevel > 0 && imageLevel < 6) {
            imageLevel--;
            batteryView.setImageLevel(imageLevel);
        }
    }
}
