package android.example.scorekeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int mScore1;
    private int mScore2;
    static final String STATE_SCORE_1 = "Team 1 Score";
    static final String STATE_SCORE_2 = "Team 2 Score";

    private TextView mViewScore1;
    private TextView mViewScore2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewScore1 = this.<TextView>findViewById(R.id.score_1);
        mViewScore2 = this.<TextView>findViewById(R.id.score_2);
        mScore1 = 0;
        mScore2 = 0;
        if (savedInstanceState != null) {
            mScore1 = savedInstanceState.getInt(STATE_SCORE_1);
            mScore2 = savedInstanceState.getInt(STATE_SCORE_2);
            mViewScore1.setText(String.valueOf(mScore1));
            mViewScore2.setText(String.valueOf(mScore2));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        outState.putInt(STATE_SCORE_1, mScore1);
        outState.putInt(STATE_SCORE_2, mScore2);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        int nightMode = AppCompatDelegate.getDefaultNightMode();
        if(nightMode == AppCompatDelegate.MODE_NIGHT_YES){
            menu.findItem(R.id.night_mode).setTitle(R.string.day_mode);
        } else{
            menu.findItem(R.id.night_mode).setTitle(R.string.night_mode);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.night_mode){
            int nightMode = AppCompatDelegate.getDefaultNightMode();
            if(nightMode == AppCompatDelegate.MODE_NIGHT_YES)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            else
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            recreate();
        }
        return true;
    }

    public void decreaseScore(View view) {
        switch(view.getId()) {
            case R.id.decreaseTeam1:
                if(mScore1 > 0) mScore1--;
                mViewScore1.setText(Integer.toString(mScore1));
                break;
            case R.id.decreaseTeam2:
                if(mScore2 > 0) mScore2--;
                mViewScore2.setText(Integer.toString(mScore2));
                break;
            default:
                break;
        }
    }

    public void increaseScore(View view) {
        switch(view.getId()) {
            case R.id.increaseTeam1:
                mScore1++;
                mViewScore1.setText(Integer.toString(mScore1));
                break;
            case R.id.increaseTeam2:
                mScore2++;
                mViewScore2.setText(Integer.toString(mScore2));
                break;
            default:
                break;
        }
    }
}
