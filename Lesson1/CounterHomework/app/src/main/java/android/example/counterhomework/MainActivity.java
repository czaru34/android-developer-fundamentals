package android.example.counterhomework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewDebug;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    int vCount;
    TextView counterTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vCount = 0;
        counterTextView = (TextView)findViewById(R.id.counter);
        if(savedInstanceState != null){
            counterTextView.setText(Integer.toString(savedInstanceState.getInt("counter")));
        }
    }

    public void count(View view) {
        vCount++;
        counterTextView.setText(Integer.toString(vCount));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(counterTextView.getVisibility() == View.VISIBLE) {
            outState.putInt("counter", vCount);
        }
    }
}
