package com.example.HelloConstraint;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private int mCount = 0;
    private TextView mShowCount;
    private TextView mZeroButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mShowCount = (TextView) findViewById(R.id.show_count);
        mZeroButton = (TextView) findViewById(R.id.button_zero);
    }

    public void showToast(View view) {
        Toast toast = Toast.makeText(this, R.string.toast_message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void countUp(View view) {
        mCount++;
        if (mShowCount != null) {
            mShowCount.setText(Integer.toString(mCount));
            mZeroButton.setBackgroundColor(Color.GREEN);
        }
        if(mCount%2 == 0) {
            view.setBackgroundColor(Color.MAGENTA);
        } else {
            view.setBackgroundColor(Color.BLUE);
        }
    }

    public void zeroCount(View view) {
        if (mCount > 0) {
            mShowCount.setText("0");
            mCount = 0;
            view.setBackgroundColor(Color.GRAY);
            mShowCount.setBackgroundColor(Color.BLUE);
        }
    }
}
